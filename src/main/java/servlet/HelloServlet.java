package servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HelloServlet extends HttpServlet {

    private String message;

    public void init() throws ServletException {
        // 初始化
        message = "Hello, First Servlet!";
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //设置response使用的码表，以控制response以什么码表向浏览器写出数据
        response.setCharacterEncoding("UTF-8");
        //指定浏览器以什么码表打开浏览器发送的数据
        response.setContentType("text/html;charset=UTF-8");
        // 转发可以携带参数，重定向不可以带参数
        request.setAttribute("message","测试消息");
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
}
